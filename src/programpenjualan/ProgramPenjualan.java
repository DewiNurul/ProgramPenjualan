/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programpenjualan;
import java.util.Scanner;

/**
 *
 * @author asus
 */
public class ProgramPenjualan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          // Deklarasi
        String namaPembeli;
        byte anggota, lanjut;
        int jumlahBarang;
        double diskon, hargaBarangsatuan, totalHarga, totalBayar = 0;

        // Membuat scanner baru
        Scanner baca = new Scanner(System.in);

        // Input
        do {
            anggota = 0;
            jumlahBarang = 0;
            diskon = 0;
            hargaBarangsatuan = 0;
            totalBayar = 0;
            System.out.println("Nama Pembeli:'");
            namaPembeli = baca.next();
            System.out.println("Jumlah Barang:");
            jumlahBarang = baca.nextInt();
            System.out.println("Harga Barang Satuan:");
            hargaBarangsatuan = baca.nextDouble();
            System.out.println("Total Harga:");
            // totalHarga = baca.nextInt();
            totalHarga = jumlahBarang * hargaBarangsatuan;
            System.out.println(totalHarga);
            // System.out.println("Apakah total harga barang > 1000000 ? 1. Yes, 2. No");
            if (totalHarga > 1000000) {
                diskon = 0.10 * totalHarga;
            }
            System.out.println("Pembelian diatas 1 jut, pelanggan akan mendapat diskon : " + diskon);
            System.out.println("Apakah pembeli merupakan anggota? 1. Yes, 2. No:");
            anggota = baca.nextByte();
            if (anggota == 1) {
                diskon += 0.10 * totalHarga;
            }

            //Proses
            totalBayar = totalHarga - diskon;
            System.out.println("Total diskon nya : " + diskon);
            System.out.println("Total bayar : " + totalBayar);
            //Output
            System.out.println("Lanjut ke customer berikut ya (1/0): ");
            lanjut = baca.nextByte();

        } while (lanjut == 1);
        System.out.println("Programselesai...");

    }
    
}
